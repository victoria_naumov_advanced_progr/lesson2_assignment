sqlite> begin transaction;
sqlite> update accounts set  balance = "-300" where id=1;
sqlite> update accounts set  balance = "1100" where id=3;
sqlite> commit
   ...> ;
sqlite> SELECT * FROM accounts;
3|1100
2|700
1|-300


sqlite> begin transaction;
sqlite> update accounts set  id=2 where balance="1200";
sqlite> update accounts set  id=1 where balance="-400";
sqlite> commit;
sqlite> SELECT * FROM accounts;
3|1100
2|800
1|-400